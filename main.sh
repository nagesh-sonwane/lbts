#!/bin/bash

DEVICE=$1

TEST_PERFORMED=0
TEST_PASSED=0
TEST_FAILED=0

function print_help() {

	echo -e "Usage: $0 [option] "
	echo -e "Options:"
	echo -e "\t--h\t\t\t\t\t: To print this help message"
	echo -e "\t--all\t\t\t\t\t: Use this to perform all tests listed as following"
	echo -e "\t--rtc\t\t\t\t\t: Use this to test RTC device driver"
	echo -e "\t--eth0\t\t\t\t\t: Use this to test eth0 port"
	echo -e "\t\t\t\t\t\t  Please make sure the port is connected to ping global ip address"
	echo -e "\t--eth1\t\t\t\t\t: Use this to test eth1 port"
	echo -e "\t\t\t\t\t\t  Please make sure the port is connected to ping global ip address"
	echo -e "\t--temp\t\t\t\t\t: Use this to perform temperature sensor test (tmp112)"
	echo -e "\t--eeprom\t\t\t\t: USe this option to test reading the EEPROM"
	echo -e "\t--serial1 \t\t\t\t: Use this to test RS232 serial port with loopback cable"
	echo -e "\t--serial2 (for F model board)\t\t: Use this to test RS232 serial port available in F model board"
	echo -e "\t\t\t\t\t\t  with loopback cable"
	echo -e "\t--serial-485-g\t\t\t\t: Use this to test rs485 with G model device board"
	echo -e "\t--i2c\t\t\t\t\t: Use this to check i2c 0 bus for probaly 56 and 60 address devices"
	echo -e "\t--spi\t\t\t\t\t: Use this to test spi driver"
	echo -e "\t--sdcard\t\t\t\t: Use this to test SDCard"
	echo -e "\t\t\t\t\t\t  Please make sure the SDcard is inserted and mounted as /media/sdcard location"
	echo -e "\t--mtd_ioctl_error\t\t\t: Use this to test mtd driver MEMGETUNIQUE ioctls failing error"
	echo -e "\t--arptables\t\t\t\t: Use this to test arptables by adding rule"
	echo -e "\n"
        echo -e "\tNote: For manual testing RS485 ports, please use 'serial-485-send' and 'serial-485-recv' test apps." 
	echo -e "\t      This is moslty useful for devices with only single RS485 port available like "F" model and DA50 boards."
	echo -e "\t      Please refer project confluence page for more details.\n"

	exit
}


if [[ ("$1" == "") || ("$1" == "--h") || ( $(echo $1 | head -c 2) != "--") ]]
then

	if [[ ("$1" == "")  || ( "$(echo $1 | head -c 2)" != "--") ]]
	then
	echo -e "ERROR: Please provide the valid argument"
	fi
	
	print_help
	
fi

function test_rtc() {

	if [ -e /sys/class/rtc/rtc0 ]

	then
		return 0
	else
		return 1 
	fi
}

if [[ "$DEVICE" == "--rtc" || "$DEVICE" == "--all" ]]
then
	# minimum to check class driver present only

	echo "Testing RTC driver.."
	
	((TEST_PERFORMED+=1))

	test_rtc
	if [[ "$?" == "0" ]]
	then
		echo "RTC driver present!!"
		echo -e "RTC driver test passed successfully !!\n"
		((TEST_PASSED+=1))
	else
		echo "Could not find RTC driver !!"
		echo -e "RTC test failed !!\n"
		((TEST_FAILED+=1))
	fi

	#moderate - check and compare time from rtc chip with system time
	#maximum - This shall include like testing ioctl and other features of RTC chip as such
fi

function test_eth0() {
	
	echo "Please make sure eth0 is up and assigned with IP address"
	
	CARIER=$(cat /sys/class/net/eth0/carrier)

	if [[ "$CARIER" == "1" ]]
	then
		echo "Connection present !!"
	else
		echo "Cable doesnt seems to be connected."
		echo "Please connect the cable and try again."
		return 1
	fi

	echo "trying to ping 8.8.8.8 to check network connection .."

	ping -W 3 -c 1 8.8.8.8 -I eth0

	if [[ "$?" == "0" ]]
	then
		return 0
	else
		return 1
	fi
}

if [[ "$DEVICE" == "--eth0" || "$DEVICE" == "--all" ]]
then

	echo "Testing eth0.."
	
	((TEST_PERFORMED+=1))
	
	test_eth0
	if [[ "$?" == "0" ]]
	then
		echo -e "Ethenet eth0 test passed successfully !!\n"
		((TEST_PASSED+=1))
	else
		echo -e "Ethernet eth0 test failed !!\n"
		((TEST_FAILED+=1))

	fi

fi

function test_eth1() {

	echo "Please make sure eth1 is up and assigned with proper IP address"

	CARIER=$(cat /sys/class/net/eth1/carrier)

	if [[ "$CARIER" == "1" ]]
	then
		echo "Connection present !!"
	else
		echo "Cable doesnt seems to be connected."
		echo "Please connect the cable and try again."
		return 1
	fi

	echo "trying to ping 8.8.8.8 to check network connection .."

	ping -W 3 -c 1 8.8.8.8 -I eth1

	if [[ "$?" == "0" ]]
	then
		return 0
	else
		return 1
	fi
}

if [[ "$DEVICE" == "--eth1" || "$DEVICE" == "--all" ]]
then
	echo "Testing eth1.."

	((TEST_PERFORMED+=1))

	test_eth1
	if [[ "$?" == "0" ]]
	then 
		echo -e "Ethenet eth1 test passed successfully !!\n"
		((TEST_PASSED+=1))
	else
		echo -e "Ethernet eth1 test failed !!\n"
		((TEST_FAILED+=1))
		
	fi
fi

function test_temp() {

	echo "Checking temperature sensor tmp112"

	SENSOR=$(cat /sys/class/hwmon/hwmon0/device/name)

	if [[ "$SENSOR" == "tmp112" ]]
	then
		echo "The temperature sensor tmp112 found"
	else
		echo "The temperature sensor tmp112 not found .. exiting"
		return 1
	fi
	
	TEMP=$(cat /sys/class/hwmon/hwmon0/temp1_input)

	echo "The temperature seems ${TEMP:0:2} celsius" 

	return 0
}

if [[ "$DEVICE" == "--temp" || "$DEVICE" == "--all" ]]
then
	echo "Testing temperature sensor.."

	((TEST_PERFORMED+=1))
		
	test_temp
	if [ "$?" == "1" ]
	then		
		echo -e "Temperature test from tmp112 ic is failed !!\n"
		((TEST_FAILED+=1))
	else
		echo -e "Temperature test passed successfull !! \n"
		((TEST_PASSED+=1))
	fi
fi

function test_eeprom {

	echo "Note that this will just read the data and will check status of read command"
	echo "We are not writing to chip in this test"

	cat /sys/class/spi_master/spi1/spi1.1/eeprom > /dev/null 2>&1

	if [ "$?" == "0" ]
	then 
		return 0
	else
		return 1
	fi
}

if [[ "$DEVICE" == "--eeprom" || "$DEVICE" == "--all" ]]
then
	echo "Testing EEPROM.."
	
	((TEST_PERFORMED+=1))
	
	if test_eeprom
	then
		echo "Able to read from EEPROM"
		echo -e "EEPROM Test is successful !!\n"
		((TEST_PASSED+=1))
	else
		echo "Could not read from EEPROM !!\n"
		echo -e "EEPROM Test failed"
		((TEST_FAILED+=1))
	fi
fi

function test_serial1 {
	
	linux-serial-test -h > /dev/null 

	if [ "$?" != "0" ]
	then 
		echo "Please install linux-serial-test utility for this test !!"
		return 1
	fi

	echo -n 121 > /sys/class/gpio/export
	echo -n out > /sys/class/gpio/gpio121/direction
	echo -n 1 > /sys/class/gpio/gpio121/value

	linux-serial-test -o 1 -i 1 -p /dev/ttyS1

	echo "Do you see the proper output (n/y) ?"
	read in
	if [ "$in" == "y" ]
	then
		return 0
	else
		return 1
	fi
}

if [[ "$DEVICE" == "--serial1" || "$DEVICE" == "--all" ]]
then
	echo "Testing serial - 1 port.."

	((TEST_PERFORMED+=1))

	if test_serial1
	then
		echo -e "Serial port 1 test successfull !!\n"
		((TEST_PASSED+=1))
	else
		echo -e "Serial port 1 test failed !!\n"
		((TEST_FAILED+=1))
	fi
fi

function test_serial2 {

	linux-serial-test -h > /dev/null

	if [ "$?" != "0" ]
	then
		echo "Please install linux-serial-test utility for this test !!"
		return 1
	fi

	echo -n 113 > /sys/class/gpio/export
	echo -n out > /sys/class/gpio/gpio113/direction
	echo -n 1 > /sys/class/gpio/gpio113/value

	linux-serial-test -o 1 -i 1 -p /dev/ttyS2

	echo "Do you see the proper output (n/y) ?"
	read in
	if [ "$in" == "y" ]
	then
		return 0
	else
		return 1
	fi
}

if [[ "$DEVICE" == "--serial2" || "$DEVICE" == "--all" ]]
then
	echo "Testing serial - 2 port.."

	((TEST_PERFORMED+=1))

	if test_serial2
	then
		echo -e "Serial port 2 test successfull !!\n"
		((TEST_PASSED+=1))
	else
		echo -e "Serial port 2 test failed !!\n"
		((TEST_FAILED+=1))
	fi
fi

function test_serial3 {

	timeout 1 ./serial-485-g

	echo "Do you see all X character on terminal (n/y) ?"
	read in
	if [ "$in" == "y" ]
	then
		return 0
	else
		return 1
	fi
}

if [[ "$DEVICE" == "--serial-485-g" || "$DEVICE" == "--all" ]]
then
	echo "Testing 485 serial ports of Flexedge "G" model board"

	((TEST_PERFORMED+=1))

	if test_serial3
	then
		echo -e "Serial port 485 test for "G" model board passed successfully !!\n"
		((TEST_PASSED+=1))
	else
		echo -e "Serial port 485 test for "G" model board failed !!\n"
		((TEST_FAILED+=1))
	fi
fi

function test_i2c {

	i2cdetect -y -r 0 >> /dev/null

	if [[ "$?" != "0" ]]
	then
		echo "Please install i2c-tools and re-run the test"
		echo "you can use following steps as example"
		echo "mkdir /var/cache"
		echo "mkdir /var/cache/apk"
		echo "apk update"
		echo -e "apk add i2c-tools\n"
		return 1
	else
#		i2cdetect -y -r 0 > i2c.txt
		
		i2cdetect -y -r 0

	        echo "Do you see if there is probably device present at 56 or 60  address (n/y) ?"
        	
		read in
        		if [ "$in" == "y" ]
        	then
                	return 0
        	else
                	return 1
        	fi

#		if [[ "$?" != "0" ]]
#		then
#			echo "Not able to detect i2c-0 bus on device"
#			return 1
#		fi

#		grep -rns "56" i2c.txt > /dev/null 2>&1
#
#		if [[ "$?" != "0" ]]
#		then
#			echo "Not able to detect i2c device with 56 address !!"
#			return 1
#		else
#			echo "The i2c device with 56 address found !!"
#		fi

#		grep -rns "60" i2c.txt > /dev/null 2>&1
#
#		if [[ "$?" != "0" ]]
#		then
#			echo "Not able to detect i2c device with 60 address !!"
#			return 1
#		else
#			echo "The i2c device with 60 address found !!"
#		fi
	fi

	return 0
}

if [[ "$DEVICE" == "--i2c" || "$DEVICE" == "--all" ]]
then
	echo "Testing i2c bus.."
	echo -e "This test may take some time..\n"

	((TEST_PERFORMED+=1))

	if test_i2c
	then
		echo -e "i2c test successfull !!\n"
		((TEST_PASSED+=1))
	else
		echo -e "i2c test failed !!\n"
		((TEST_FAILED+=1))
	fi
fi

function test_spi {

	if [[ -d /sys/bus/spi/drivers/at25/spi1.1/ ]]

	then
		return 0
	else
		return 1 
	fi
}

if [[ "$DEVICE" == "--spi" || "$DEVICE" == "--all" ]]
then
	echo "Testing spi driver !!"

	((TEST_PERFORMED+=1))

	if test_spi
	then
		echo -e "spi test successfull !!\n"
		((TEST_PASSED+=1))
	else
		echo -e "spi test Failed !!\n"
		((TEST_FAILED+=1))
	fi
fi

function test_sdcard {
	
	((TEST_PERFORMED+=1))

	MOUNTPOINT="/media/sdcard"

	grep -qs "$MOUNTPOINT" /proc/mounts
	if [[ "$?" == "0" ]]
	then
		cp main.sh /media/sdcard

		if [[ "$?" == "0" ]]
		then
			return 0
		else
			return 1
		fi
	else
		echo "Please insert and mount sdcard and re-run the test"
		echo "You can use following example steps to mount the sdcard"
		echo "mkdir /media/sdcard"
		echo "mount /dev/mmcblk0p1 /media/sdcard"
		
		return 1
	fi
}

if [[ "$DEVICE" == "--sdcard" || "$DEVICE" == "--all" ]]
then
	echo "Testing SDCard"
	
	if test_sdcard
	then
		echo -e "SDCard test passed successfully !!\n"
		rm /media/sdcard/main.sh
		((TEST_PASSED+=1))
	else
		echo -e "SDCard test failed !!\n"
		((TEST_FAILED+=1))
	fi
fi

function mtd_ioctl_error {

	./mtd-ioctl

	if [[ "$?" == "0" ]]
	then
		return 0
	else 
		return 1
	fi
}

if [[ "$1" == "--mtd_ioctl_error" || "$1" == "--all" ]]
then
	echo  -e "Testing mtd_ioctl_error \n"

	((TEST_PERFORMED+=1))
	
	if mtd_ioctl_error
	then
		echo -e "mtd_ioctl_error test passed successfully !!\n"
		((TEST_PASSED+=1))
	else
		echo -e "mtd_ioctl_error test failed !!\n"
		((TEST_FAILED+=1))
	fi
fi

if [[ "$1" == "--arptables" || "$1" == "--all" ]]
then
	echo -e "Testing if arptables is working by adding rule"

	((TEST_PERFORMED+=1))
	
	arptables -v -A INPUT -i eth0 --src-mac 00:11:22:33:44:55 -j DROP

	if [[ "$?" == "0" ]]
	then
		echo -e "arptables test passed successfully\n"
		((TEST_PASSED+=1))
		
		echo -e "removing the added rule"	
		arptables -v -D INPUT -i eth0 --src-mac 00:11:22:33:44:55 -j DROP
	else
		echo -e "arptables test failed\n"
		((TEST_FAILED+=1))
	fi
fi

if [[ ( "$TEST_PERFORMED" == "0" ) && ( "$1" != "--h" ) ]]
then
	echo "No valid device provided"
	echo "Please use --h for help"
else
echo -e "\n***************************************************************"
echo "Total test performed = $TEST_PERFORMED"
echo "Total test passed    = $TEST_PASSED"
echo "Total test failed    = $TEST_FAILED"
echo -e "***************************************************************\n"

fi
