/*
	Test utility sends data using rs485 port
	
 */

#include <fcntl.h>
#include <linux/serial.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <termios.h>
#include <unistd.h>

typedef unsigned char BYTE;
static	void	*RecieveData(void *);
static	bool	RecvFrame(int , int );
static	int		OpenPort(char const *name);
static	bool	SetGpio(int gpio, int state);
static	void	SetupPort(char *);

void SetupPort(char *port)
{
	system("echo setting up port");
	system("echo -n 113 > /sys/class/gpio/export");
	system("echo -n out > /sys/class/gpio/gpio113/direction");
	system("echo -n 1 > /sys/class/gpio/gpio113/value");

	system("echo -n 112 > /sys/class/gpio/export");
	system("echo -n out > /sys/class/gpio/gpio112/direction");
	system("echo -n 1 > /sys/class/gpio/gpio112/value");

	system("echo -n 12 > /sys/class/gpio/export");
	system("echo -n out > /sys/class/gpio/gpio12/direction");
	system("echo -n 0 > /sys/class/gpio/gpio12/value");

	system("echo -n 13 > /sys/class/gpio/export");
	system("echo -n out > /sys/class/gpio/gpio13/direction");
	system("echo -n 0 > /sys/class/gpio/gpio13/value");
}

int main(int argc, char **argv)
{

	pthread_t t1, t2;

	if(argv[1]==NULL) {
		printf("Please provide port name\n");
		return 0;
	}

	printf("Recieve Data from port: %s", argv[1]);

	SetupPort(argv[1]);
	
	pthread_create(&t2, nullptr, RecieveData, argv[1]);
	pthread_join(t2, nullptr);

	return 0;
}

static void *RecieveData(void *port)
{
	int fd = OpenPort((char *)port);

	for( ;;) {

		 RecvFrame(fd, 500); 
	}

	close(fd);

	return nullptr;
}

static bool RecvFrame(int fd, int ms)
{
		int rs=0, rp=0;
		BYTE rx[128];

		for( ;;) {

			fd_set set;
			FD_ZERO(&set);
			FD_SET(fd, &set);

			timeval tv = { 0 };
			tv.tv_usec = 1000 * ms;

			if( select(fd+1, &set, nullptr, nullptr, ms ? &tv : nullptr) > 0 ) {

				BYTE b[64];

				int n = read(fd, b, sizeof(b));

//				printf("Recieved Frame %d bytes: %s\n",n,b);

				for(int i=0;i<=n;i++) {
					printf("%c ",b[i]);
				}

			}

		}

		return true;
}

static int OpenPort(char const *name)
{
	int fd = open(name, O_RDWR | O_NOCTTY);

	if( fd >= 0 ) {

		struct termios cfg = { 0 };

		cfg.c_iflag  = IGNBRK;
		cfg.c_cflag  = CREAD | CLOCAL | B9600;
		cfg.c_cflag |= CS8;
		cfg.c_ispeed = B9600;
		cfg.c_ospeed = B9600;

		ioctl(fd, TCSETS, &cfg);

		struct serial_rs485 r = { 0 };

		r.delay_rts_after_send	= 0;
		r.delay_rts_before_send = 0;
		r.flags			= SER_RS485_ENABLED | SER_RS485_RTS_AFTER_SEND;

		ioctl(fd, TIOCSRS485, &r);

		return fd;
	}

	return -1;
}

static bool SetGpio(int gpio, int state)
{
	char name[64];

	sprintf(name, "/sys/class/gpio/gpio%d/value", gpio);

	int fd = open(name, O_WRONLY, 0);

	if( fd >= 0 ) {

		char cData = state ? '1' : '0';

		write(fd, &cData, sizeof(cData));

		close(fd);

		return true;
	}

	return false;
}
